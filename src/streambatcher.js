const debug = require('debug')('scheduler')

function StreamBatcher (addToBatchFn = x => x, interval = 1000, maxbatch = 1000) {
  let delay = interval
  let batchHandlingInProgress = false

  let inprogressCount = 0

  function streambatchFactory (fn, closeHandler = () => { debug(`closed scheduler`) }) {
    let done = false
    let buffer = []

    function schedule (value) {
        debug(`schedule ${value}`)
        buffer.push(addToBatchFn(value))
        if (done) {
            action()
        }
    }

    let count = 0
    async function action () {
        debug('creating scheduler')
        inprogressCount++
        debug(`Inprogress ${inprogressCount}`)
        count = 0
        done = false
        while (!done) {
            await new Promise((resolve) => setTimeout(() => resolve(), delay))
            if (batchHandlingInProgress) continue
            debug(`batch action attempt ${count}`)
            if (buffer.length > 0) {
                while (buffer.length > 0) {
                    let batch = buffer.slice(0, maxbatch)
                    buffer = buffer.slice(maxbatch)
                    batchHandlingInProgress = true
                    await fn(batch)
                    batchHandlingInProgress = false
                }
                buffer = []
                count = 0
            } else {
                count++
            }
            if (count > 3) {
                debug(`closing scheduler`)

                done = true
            }
        }
        inprogressCount--
        debug(`Inprogress ${inprogressCount}`)
        closeHandler()
    }
    action()
    return schedule
}

  function batchInProgress () {
        debug(`Inprogress ${inprogressCount}`)
      return inprogressCount > 0
  }

  return {
      batchInProgress,
      streambatchFactory
  }
}

module.exports = StreamBatcher
