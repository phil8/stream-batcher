import { describe } from 'riteway'
import StreamBatcher from '../index.js'
const debug = require('debug')('test')

// let processed = 0

// let entity = {
//   afunction: function (args) {
//     debug('Function')
//     debug(args)
//     processed = processed + args.length
//   }
// }

// let slowBatchHandler = async (args) => {
//   console.log('slowA')
//   await new Promise((resolve) => setTimeout(() => resolve(), 10000))
//   console.log('slowB')
//   entity.afunction(args)
// }

describe('Single Schedule', async (assert) => {
  let batchCount = 0
  let slowBatchHandler = async (args) => {
    // await new Promise((resolve) => setTimeout(() => resolve(), 10000))
    batchCount++
  }

  let { batchInProgress, streambatchFactory } = StreamBatcher(x => x, 50, 2)
  let schedule = streambatchFactory(slowBatchHandler)

  schedule('1')

  await new Promise((resolve) => setTimeout(() => resolve(), 500))

  assert({
    given: 'a StreamBatcher ',
    should: 'produce a batch',
    actual: `${batchCount} ${batchInProgress()}`,
    expected: '1 false'
  })
})

describe.only('Multiple schedule', async (assert) => {
  let batchCount = 0
  let slowBatchHandler = async (args) => {
    // await new Promise((resolve) => setTimeout(() => resolve(), 10000))
    batchCount++
    debug(`receiving batch ${args.length}`)
  }

  let { streambatchFactory } = StreamBatcher(x => x, 100, 2)
  let schedule = streambatchFactory(slowBatchHandler)

  schedule('1')
  schedule('2')

  await new Promise((resolve) => setTimeout(() => resolve(), 500))

  schedule('3')
  schedule('4')
  schedule('5')

  await new Promise((resolve) => setTimeout(() => resolve(), 1000))

  assert({
      given: 'a StreamBatcher and some scheduled values',
      should: 'the values be processed',
      actual: batchCount,
      expected: 3
  })

  schedule('6')

  await new Promise((resolve) => setTimeout(() => resolve(), 500))

  assert({
    given: 'a StreamBatcher and some additional scheduled values',
    should: 'the additional values be processed',
    actual: batchCount,
    expected: 4
  })
})

describe('Concurrent Batch Handling', async (assert) => {
  let batchConcurrency = 0
  let maxBatchCount = 0
  let slowBatchHandler = async (args) => {
    batchConcurrency++
    debug(`receiving batch ${args.length}`)
    await new Promise((resolve) => setTimeout(() => resolve(), 500))
    maxBatchCount = maxBatchCount < batchConcurrency ? batchConcurrency : maxBatchCount
    debug(`received batch ${args.length}`)
    batchConcurrency--
  }

  let { streambatchFactory } = StreamBatcher(x => x, 10, 2)
  let schedule = streambatchFactory(slowBatchHandler)

  schedule('1')

  await new Promise((resolve) => setTimeout(() => resolve(), 600))

  schedule('2')

  await new Promise((resolve) => setTimeout(() => resolve(), 1000))

    assert({
    given: ' slow batch handling',
    should: 'Concurrent batch count is 1',
    actual: maxBatchCount,
    expected: 1
  })
})
